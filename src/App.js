import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Signup from './pages/Signup'

class App extends Component {
  render(){
    return (      
        <BrowserRouter>
          <div className="App">
            <Switch>
              <Route exact path='/' component={Signup} />
            </Switch>
          </div>          
        </BrowserRouter>
      
    );
  }
}

export default App;
