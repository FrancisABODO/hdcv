import React, { Component }  from 'react'
import '../styles/Signup.css'
import Input from '../components/Input/Input'
import Bouton from '../components/Bouton/Bouton'

class Signup extends Component {
    
    render () {
        return (
            <div className="container-sign">
                <h1 className="sign-title">HCV</h1>
                <Input type={"text"} placeholder={"Nom d'utilisateur"} />
                <Input type={"password"} placeholder={"Mot de passe"} />
                <Bouton />
            </div>
        );
    }

    
}

export default Signup