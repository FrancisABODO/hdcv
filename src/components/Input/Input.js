import React, { Component }  from 'react'
import '../Input/Input.css'

class InputName extends Component {
    render () {
        return (
            <div className="container-input">
                <input name="username" type={this.props.type} className="ínput-name" placeholder={this.props.placeholder} />
            </div>
        );
    }
}

export default InputName